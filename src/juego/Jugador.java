package juego;

/*!  \class Jugador
 *   Una clase que representa a un jugador de sumaQuince, en esta se
 *   guardan los datos del jugador junto con los metodos para modificar debidamente
 *   esos datos.
 */
public class Jugador {

    ///Array estatico que guarda los numeros seleccionador por el jugador
    int[] suma;
    /// entero auxiliar que se usa para marcar la posicion de un nuevo numero entrante
    int pos = 0;
    /// String que guarda el nombre del jugador
    String nombre;

    /*!
     * \brief Constructor que inicializa los 3 elementos de la suma del jugador en 0.
     */
    public Jugador() {
        suma=new int[3];
        suma[0] = 0;
        suma[1] = 0;
        suma[2] = 0;
    }

    /*!
     * \brief Funcion set que asigna al atributo nombre un String entrante
     * \param nombre  String que indica el nombre del jugador
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /*!
     * \brief Funcion set que asigna a una posicion del atributo suma un entero entrante
     * \param num entero que indica el numero a guardar en la suma
     */
    public void setSuma(int num) {

        suma[pos] = num;
        pos++;

    }

    /*!
     * \brief Funcion get que retorna el arreglo suma
     */
    public int[] getSuma() {
        return suma;
    }

    /*!
     * \brief Funcion get que retorna el nombre del jugador
     */
    public String getNombre() {
        return nombre;
    }

    /*!
     * \brief Funcion que reemplaza en la posicion de un numero anterior entrante, un nuevo numero entrante.
     * \param numA  entero que representa el numero a reemplazar
     * \param numN  entero que representa el nuevo numero a asignar
     */
    public boolean cambiarNum(int numA, int numN) {
        for (int i = 0; i < 3; i++) {
            if (suma[i] == numA) {
                suma[i] = numN;
                return true;
            }
        }
        return false;
    }
    /*!
     * \brief Funcion que recibe un entero y lo busca en el arreglo suma del jugador.
     * \param num  entero que representa el numero a buscar en el arreglo
     */
    public boolean encontrarNum(int num){
        for (int i = 0; i < 3; i++) {
            if(num==suma[i]){
                return true;
            }
        }
        return false;
    }
}
