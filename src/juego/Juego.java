package juego;

/**
 *
 * @author Microsoft
 */

/*!  \class Juego
 *   Una clase que representa el juego suma15, contiene dos objetos de 
 *   la clase Jugador y sus metodos validan las elecciones hechas por los 
 *   jugadores; Con el metodo ganar determina el momento en que se cumplen
 *   las condiciones para la victoria.
 */
public class Juego {

    ///Clase Jugador que guarda los datos del jugador 1
    Jugador p1;
    ///Clase Jugador que guarda los datos del jugador 2
    Jugador p2;

    /*!
     * \brief Constructor que recibe dos objetos de la clase Jugador y los agrega a los atributos.
     */
    public Juego(Jugador p1,Jugador p2) {
        this.p1=p1;
        this.p2=p2;
    }

    /*!
     * \brief Funcion que dado un numero retorna un booleano que valida
     * si el numero no esta repetido en alguno de los arreglos de los jugadores(true) o en su defecto lo esta (false)
     * \param num  entero que indica el numero a validar
     */
    public boolean validarNum(int num) {
        for (int i = 0; i < 3; i++) {
            if (num == p1.suma[i] || num == p2.suma[i]) {
                return false;
            }
        }
        return true;
    }

    /*!
     * \brief Funcion que dado 4 numeros que representan dos posiciones en una matriz 
     * retorna un booleano que valida si la nueva posicion cumple(true) la condicion de ser contigua 
     * a la anterior
     * \param x1 entero que indica la columna de la antigua posicion
     * \param x2 entero que indica la columna de la nueva posicion
     * \param y1 entero que indica la fila de la antigua posicion
     * \param y2 entero que indica la fila de la nueva posicion
     */
    public boolean validarMovimientoNum(int x1, int x2, int y1, int y2) {
        if (x1 == x2 && (y1 == (y2 + 1) || y1 == (y2 - 1))) {
            return true;
        }
        if (y1 == y2 && (x1 == (x2 + 1) || x1 == (x2 - 1))) {
            return true;
        }
        return false;
    }
/*!
     * \brief Funcion que dado un arreglo valida si los 3 numeros que contiene suman 15   
     * retorna un booleano que representa la victoria(true)
     * \param arr el arreglo a validar
     */
    public boolean ganar(int[] arr) {
        int suma = arr[0] + arr[1] + arr[2];
        if (suma == 15 && (arr[0] != 0 && arr[1] != 0 && arr[2] != 0)) {
            return true;
        }
        return false;
    }

    /*
     * \brief Para hacer pruebas se tiene el main...  
     * \param args es una lista de Strings conteniendo los
     *  argumentos usados por el programa.
     */
    public static void main(String[] args) {
    SumaQuince juego=new SumaQuince();
    juego.setVisible(true);
    }

}
